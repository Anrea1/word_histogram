<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>main</name>
    <message>
        <location filename="main.qml" line="11"/>
        <source>File words histogram</source>
        <translation>Гистограмма количества слов в файле</translation>
    </message>
    <message>
        <location filename="main.qml" line="22"/>
        <source>Select text file:</source>
        <oldsource>Select file:</oldsource>
        <translation>Выберите файл:</translation>
    </message>
    <message>
        <location filename="main.qml" line="28"/>
        <source>Select text file by browse button</source>
        <translation>Выберите текстовый файл кнопкой &lt;Выбрать...&gt;</translation>
    </message>
    <message>
        <location filename="main.qml" line="34"/>
        <source>Browse...</source>
        <translation>Выбрать...</translation>
    </message>
    <message>
        <location filename="main.qml" line="42"/>
        <source>Number of words in histogram:</source>
        <translation>Количество слов в гистограмме:</translation>
    </message>
    <message>
        <location filename="main.qml" line="54"/>
        <source>Delay of reading file(ms):</source>
        <translation>Задержка чтения файла(мс):</translation>
    </message>
    <message>
        <location filename="main.qml" line="69"/>
        <source>Start</source>
        <translation>Запуск</translation>
    </message>
    <message>
        <location filename="main.qml" line="69"/>
        <source>Stop</source>
        <translation>Прервать</translation>
    </message>
    <message>
        <location filename="main.qml" line="82"/>
        <source>No data</source>
        <translation>Нет данных</translation>
    </message>
</context>
</TS>
