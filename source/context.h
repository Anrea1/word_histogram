#ifndef CONTEXT_H
#define CONTEXT_H

#include <QObject>

class Context: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QObject* arrayModel READ arrayModel CONSTANT)
    Q_PROPERTY(bool processing READ processing NOTIFY processingChanged)
    Q_PROPERTY(float progress READ progress NOTIFY progressChanged)
public:
    Context(QObject* parent = nullptr);
    ~Context() override;

    // QML API interface
    QObject* arrayModel() const;
    bool processing() const;
    float progress() const;

    Q_INVOKABLE void start(const QString& file, int wordsNumber, int delayMs);
    Q_INVOKABLE void stop();

signals:
    void processingChanged(bool);
    void progressChanged(float);

private:
    class Impl;
    QScopedPointer<Impl> d;
};

#endif//CONTEXT_H
