#include "data_filter_worker.h"

#include <set>

namespace
{
    struct WordCount
    {
        CountType count;
        QString word;

        bool operator <(const WordCount& wordCount) const
        {
            return count == wordCount.count ? word > wordCount.word : count < wordCount.count;
        }
        operator ResultArray::value_type() const
        {
            return { word, count };
        }
    };
}

class DataFilterWorker::Impl
{
public:
    unsigned int firstMaxWordsCount;
    QMap<QString, CountType> map;
    std::set<::WordCount> firstSet;
};

DataFilterWorker::DataFilterWorker(unsigned int firstMaxWordsCount) :
    d(new Impl)
{
    d->firstMaxWordsCount = firstMaxWordsCount;
}

DataFilterWorker::~DataFilterWorker()
{
}

void DataFilterWorker::obtainResultMap(const ResultMap& resultMap)
{
    bool setChanged = false;
    for(auto it = resultMap.begin(), end = resultMap.end(); it != end; ++it)
    {
        auto find = d->map.find(it.key());
        if(find == d->map.end())
        {
            find = d->map.insert(it.key(), 0);
        }
        auto oldValue = find.value();
        auto newValue = ++find.value();

        if(d->firstSet.size() < d->firstMaxWordsCount or
           newValue >= d->firstSet.begin()->count)
        {
            setChanged = true;

            if(oldValue > 0)
            {
                d->firstSet.erase({ oldValue, it.key() });
            }

            d->firstSet.insert({ newValue, it.key() });

            if(d->firstSet.size() > d->firstMaxWordsCount)
            {
                auto begin = d->firstSet.begin();
                d->firstSet.erase(begin);
            }
        }
    }

    if(setChanged)
    {
        ResultArray array;
        array.reserve(d->firstSet.size());
        std::transform(d->firstSet.cbegin(), d->firstSet.cend(), std::back_inserter(array),
                       [](const ::WordCount& wordCount) { return wordCount; });
        emit resultReady(array);
    }
}

