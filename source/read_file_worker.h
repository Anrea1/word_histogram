#ifndef READ_FILE_WORKER_H
#define READ_FILE_WORKER_H

#include <QObject>

#include "read_traits.h"

class ReadFileWorker : public QObject
{
    Q_OBJECT
public:
    ReadFileWorker(QObject* parent = nullptr);
    ~ReadFileWorker() override;

public slots:
    void readFile(const QString& fileName, unsigned int delayMs);

signals:
    void progressChanged(float);
    void resultReady(ResultMap);
    void readingFinished();

private slots:
    void nextStep();

private:
    class Impl;
    QScopedPointer<Impl> d;
};

#endif//READ_FILE_WORKER_H
