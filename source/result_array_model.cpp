#include "result_array_model.h"

class ResultArrayModel::Impl
{
public:
    ResultArray array;
    float maxCount = 0;

    static void sortArray(ResultArray& array)
    {
        std::sort(array.begin(), array.end(), [](const ResultPair& first, const ResultPair& second)
        {
            return first.first < second.first;
        });
    }
};

ResultArrayModel::ResultArrayModel(QObject* parent) :
    QAbstractListModel(parent), d(new Impl)
{
}

ResultArrayModel::~ResultArrayModel()
{
}

void ResultArrayModel::clear()
{
    this->beginResetModel();
    d->array.clear();
    this->endResetModel();
    this->updateHasData();
}

bool ResultArrayModel::hasData() const
{
    return !d->array.isEmpty();
}

int ResultArrayModel::maxCount() const
{
    return d->maxCount;
}

int ResultArrayModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
    {
        return 0;
    }
    return d->array.size();
}

QVariant ResultArrayModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
    {
        return QVariant();
    }
    return get(index.row(), static_cast<Roles>(role));
}

QVariant ResultArrayModel::get(int index, ResultArrayModel::Roles role) const
{
    if (index < 0 or index >= d->array.size())
        return QVariant();

    const auto& wordCount = d->array.at(index);
    switch (role)
    {
    case Roles::WordRole:
        return wordCount.first;
    case Roles::CountRole:
        return wordCount.second;
    }
    return QVariant();
}

QHash<int, QByteArray> ResultArrayModel::roleNames() const
{
    QHash<int, QByteArray> roles;
    roles[Roles::WordRole] = "text";
    roles[Roles::CountRole] = "count";
    return roles;
}

void ResultArrayModel::setArray(ResultArray array)
{
    bool updateHasData = d->array.isEmpty() != array.isEmpty();
    auto newMaxCount = array.isEmpty() ? 0 : array.last().second;
    if(d->maxCount != newMaxCount)
    {
        d->maxCount = newMaxCount;
        emit maxCountChanged(newMaxCount);
    }
    Impl::sortArray(array); // Sort array by alphabet

    // Smart update model data
    int i = 0;
    auto it = d->array.begin();
    auto newIt = array.cbegin(), newEnd = array.cend();

    while(newIt != newEnd)
    {
        if(it == d->array.end())
        {
            this->beginInsertRows(QModelIndex(), i, i);
            it = d->array.insert(it, *newIt);
            this->endInsertRows();
            ++newIt;
            ++it;
            ++i;
        }
        else if(*newIt != *it)
        {
            if(newIt->first != it->first)
            {
                if(newIt->first < it->first)
                {
                    this->beginInsertRows(QModelIndex(), i, i);
                    it = d->array.insert(it, *newIt);
                    this->endInsertRows();
                    ++newIt;
                    ++it;
                    ++i;
                }
                else if(newIt->first > it->first)
                {
                    this->beginRemoveRows(QModelIndex(), i, i);
                    it = d->array.erase(it);
                    this->endRemoveRows();
                }
            }
            else
            {
                it->second = newIt->second;
                emit dataChanged(this->index(i), this->index(i), { CountRole });
                ++newIt;
                ++it;
                ++i;
            }
        }
        else
        {
            ++newIt;
            ++it;
            ++i;
        }
    }
    if(it != d->array.end())
    {
        this->beginRemoveRows(QModelIndex(), i, d->array.size() - 1);
        d->array.erase(it, d->array.end());
        this->endRemoveRows();
    }

    if(updateHasData)
    {
        this->updateHasData();
    }
}

void ResultArrayModel::updateHasData()
{
    emit hasDataChanged(this->hasData());
}
