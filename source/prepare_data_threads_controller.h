#ifndef PREPARE_DATA_THREADS_CONTROLLER_H
#define PREPARE_DATA_THREADS_CONTROLLER_H

#include <QThread>

#include "prepare_traits.h"

class PrepareDataThreadsController : public QObject
{
    Q_OBJECT
public:
    PrepareDataThreadsController(QObject* parent = nullptr);
    ~PrepareDataThreadsController() override;

    bool processing() const;
    float progress() const;

public slots:
    void readFile(const QString& fileName,
                  unsigned int firstMaxWordsCount,
                  unsigned int delayMs);
    void stop();

signals:
    void processingChanged(bool);
    void progressChanged(float);
    void resultReady(ResultArray);

private slots:
    void onProgressChanged(float progress);

private:
    void updateProcesssing();

    class Impl;
    QScopedPointer<Impl> d;
};

#endif//PREPARE_DATA_THREADS_CONTROLLER_H
