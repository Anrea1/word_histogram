#include "prepare_data_threads_controller.h"

#include <QThread>

#include "read_file_worker.h"
#include "data_filter_worker.h"

class PrepareDataThreadsController::Impl
{
public:
    float progress = 0;
    QThread* readThread = nullptr;
    QThread* filterThread = nullptr;
    ReadFileWorker* readWorker = nullptr;
    DataFilterWorker* filterWorker = nullptr;
};

PrepareDataThreadsController::PrepareDataThreadsController(QObject* parent) :
    QObject(parent), d(new Impl)
{
    qRegisterMetaType<ResultMap>("ResultMap");
    qRegisterMetaType<ResultArray>("ResultArray");
}

PrepareDataThreadsController::~PrepareDataThreadsController()
{
    this->stop();
}

bool PrepareDataThreadsController::processing() const
{
    return d->readThread or d->filterThread;
}

float PrepareDataThreadsController::progress() const
{
    return d->progress;
}

void PrepareDataThreadsController::readFile(const QString& fileName,
                                            unsigned int firstMaxWordsCount,
                                            unsigned int delayMs)
{
    if(d->readThread or d->filterThread) return;

    this->onProgressChanged(0);

    d->readThread = new QThread(this);
    d->filterThread = new QThread(this);
    d->readWorker = new ReadFileWorker();
    d->filterWorker = new DataFilterWorker(firstMaxWordsCount);
    d->readWorker->moveToThread(d->readThread);
    d->filterWorker->moveToThread(d->filterThread);

    connect(d->readThread, &QThread::started, [this, fileName, delayMs]()
    {
        d->readWorker->readFile(fileName, delayMs);
    });
    connect(d->readThread, &QThread::finished, [this]()
    {
        d->readWorker->deleteLater();
        d->readThread->deleteLater();
        d->readThread = nullptr;

        this->updateProcesssing();
    });
    connect(d->filterThread, &QThread::started, [this]()
    {
        d->readThread->start();
    });
    connect(d->filterThread, &QThread::finished, [this]()
    {
        if(d->readThread and d->readThread->isRunning())
        {
            d->readThread->quit();
            d->readThread->wait();
        }

        d->filterWorker->deleteLater();
        d->filterThread->deleteLater();
        d->filterThread = nullptr;

        this->updateProcesssing();
    });
    connect(d->readWorker, &ReadFileWorker::readingFinished, this, &PrepareDataThreadsController::stop);
    connect(d->readWorker, &ReadFileWorker::resultReady, d->filterWorker, &DataFilterWorker::obtainResultMap);
    connect(d->readWorker, &ReadFileWorker::progressChanged, this, &PrepareDataThreadsController::onProgressChanged);
    connect(d->filterWorker, &DataFilterWorker::resultReady, this, &PrepareDataThreadsController::resultReady);
    d->filterThread->start();

    this->updateProcesssing();
}

void PrepareDataThreadsController::stop()
{
    if(d->filterThread and d->filterThread->isRunning())
    {
        d->filterThread->quit();
        d->filterThread->wait();
    }
}

void PrepareDataThreadsController::onProgressChanged(float progress)
{
    emit progressChanged(d->progress = progress);
}

void PrepareDataThreadsController::updateProcesssing()
{
    emit processingChanged(this->processing());
}
