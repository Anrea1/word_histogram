#ifndef DATA_FILTER_WORKER_H
#define DATA_FILTER_WORKER_H

#include <QObject>

#include "read_traits.h"
#include "prepare_traits.h"

class DataFilterWorker : public QObject
{
    Q_OBJECT
public:
    DataFilterWorker(unsigned int firstMaxWordsCount);
    ~DataFilterWorker() override;

signals:
    void resultReady(ResultArray);

public slots:
  void obtainResultMap(const ResultMap& resultMap);

private:
  class Impl;
  QScopedPointer<Impl> d;
};

#endif//DATA_FILTER_WORKER_H
