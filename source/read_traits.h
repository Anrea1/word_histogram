#ifndef READ_TRAITS_H
#define READ_TRAITS_H

#include <QMap>
#include <QMetaType>

#include "common_traits.h"

using ResultMap = QMap<QString, CountType>;

Q_DECLARE_METATYPE(ResultMap)

#endif//READ_TRAITS_H
