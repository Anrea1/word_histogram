#include "context.h"

#include <QUrl>
#include <QDir>

#include "source/prepare_data_threads_controller.h"
#include "source/result_array_model.h"

class Context::Impl
{
public:
    ResultArrayModel* arrayModel;
    PrepareDataThreadsController* prepareController;
};

Context::Context(QObject* parent) :
    QObject(parent), d(new Impl)
{
    d->arrayModel = new ResultArrayModel(this);
    d->prepareController = new PrepareDataThreadsController(this);

    connect(d->prepareController, &PrepareDataThreadsController::processingChanged,
            this, &Context::processingChanged);
    connect(d->prepareController, &PrepareDataThreadsController::progressChanged,
            this, &Context::progressChanged);
    connect(d->prepareController, &PrepareDataThreadsController::resultReady,
            d->arrayModel, &ResultArrayModel::setArray);
}

Context::~Context()
{
}

QObject* Context::arrayModel() const
{
    return d->arrayModel;
}

bool Context::processing() const
{
    return d->prepareController->processing();
}

float Context::progress() const
{
    return d->prepareController->progress();
}

void Context::start(const QString& file, int wordsNumber, int delayMs)
{
    const QUrl& url(file);
    d->arrayModel->clear();
    d->prepareController->readFile(url.isLocalFile() ? QDir::toNativeSeparators(url.toLocalFile())
                                                     : file, wordsNumber, delayMs);
}

void Context::stop()
{
    d->prepareController->stop();
}
