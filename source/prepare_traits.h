#ifndef PREPARE_TRAITS_H
#define PREPARE_TRAITS_H

#include <QVector>
#include <QMetaType>

#include "common_traits.h"

using ResultPair = QPair<QString, CountType>;
using ResultArray = QVector<ResultPair>;

Q_DECLARE_METATYPE(ResultArray)

#endif//PREPARE_TRAITS_H
