#ifndef RESULT_ARRAY_MODEL_H
#define RESULT_ARRAY_MODEL_H

#include <QAbstractListModel>

#include "prepare_traits.h"

class ResultArrayModel: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(bool hasData READ hasData NOTIFY hasDataChanged)
    Q_PROPERTY(int maxCount READ maxCount NOTIFY maxCountChanged)
public:
    enum Roles
    {
        WordRole = Qt::UserRole + 1,
        CountRole
    };
    ResultArrayModel(QObject* parent = nullptr);
    ~ResultArrayModel() override;

    void clear();

    // QML API interface
    bool hasData() const;
    int maxCount() const;

    // QAbstractItemModel interface
    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role) const override;
    QVariant get(int index, Roles role) const;
    QHash<int, QByteArray> roleNames() const override;

signals:
    void hasDataChanged(bool);
    void maxCountChanged(int);

public slots:
    void setArray(ResultArray array);

private:
    void updateHasData();

    class Impl;
    QScopedPointer<Impl> d;
};

#endif//RESULT_ARRAY_MODEL_H
