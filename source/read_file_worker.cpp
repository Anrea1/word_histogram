#include "read_file_worker.h"

#include <QFile>
#include <QThread>
#include <QTextStream>
#include <QTimer>

namespace
{
    const unsigned int ENDL_SIZE_IN_BYTES = QString("\n").toUtf8().size();
}

class ReadFileWorker::Impl
{
public:
    QFile* file = nullptr;
    QTextStream* stream = nullptr;
    double fileSize = 0;
    uint64_t readSize = 0;
    unsigned int delayMs = 0;

    QTimer* timer = nullptr;
};

ReadFileWorker::ReadFileWorker(QObject* parent) :
    QObject(parent), d(new Impl)
{
}

ReadFileWorker::~ReadFileWorker()
{
    if(d->stream)
    {
        d->file->close();
    }
}

void ReadFileWorker::readFile(const QString& fileName, unsigned int delayMs)
{
    if(d->file) return;

    d->delayMs = delayMs;
    d->fileSize = d->readSize = 0;
    d->file = new QFile(fileName, this);
    if(d->file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
        d->fileSize = d->file->size();
        d->stream = new QTextStream(d->file);
        d->timer = new QTimer(this);
        d->timer->setSingleShot(true);
        connect(d->timer, &QTimer::timeout, this, &ReadFileWorker::nextStep);

        this->nextStep();
    }
}

void ReadFileWorker::nextStep()
{
    if(d->stream->atEnd())
    {
        emit readingFinished();
        return;
    }

    QString rawLine = d->stream->readLine();
    d->readSize += rawLine.toUtf8().size() + ::ENDL_SIZE_IN_BYTES;

    auto line = rawLine.toLower();

    QRegExp rx("\\w+");
    int pos = 0;

    ResultMap map;
    while ((pos = rx.indexIn(line, pos)) != -1)
    {
        pos += rx.matchedLength();
        for(const auto& cap: rx.capturedTexts())
        {
            ++map[cap];
        }
    }
    if(!map.isEmpty()) emit resultReady(map);

    if(d->fileSize > 0) emit progressChanged(d->readSize / d->fileSize);

    d->timer->start(d->delayMs);
}
