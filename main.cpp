#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include <QDebug>
#include <QTranslator>
#include <QDir>

#include "source/context.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName("MyCompany");
    QCoreApplication::setOrganizationDomain("www.mycompany.com");

    QGuiApplication app(argc, argv);

    QTranslator translator;
    if(translator.load(QLocale().name(), "."))
    {
        qApp->installTranslator(&translator);
    }
    else
    {
        qDebug() << "Can\'t find" << QLocale().name() << "qm-file in" << QDir::currentPath();
    }

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl)
    {
        if (!obj && url == objUrl) QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    Context contextObject;
    QQmlEngine::setObjectOwnership(&contextObject, QQmlEngine::CppOwnership);
    engine.rootContext()->setContextObject(&contextObject);
    engine.load(url);

    return app.exec();
}
