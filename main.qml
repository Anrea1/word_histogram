import QtQuick 2.11
import QtQuick.Controls 2.4
import QtQuick.Window 2.11
import QtQuick.Layouts 1.3
import QtQuick.Dialogs 1.2

Window {
    visible: true
    width: 640
    height: 480
    title: qsTr("File words histogram")
    minimumWidth: layout.implicitWidth + layout.anchors.margins * 2
    minimumHeight: layout.implicitHeight + layout.anchors.margins * 2

    ColumnLayout {
        id: layout
        anchors.fill: parent
        anchors.margins: 5
        RowLayout {
            Layout.fillWidth: true
            Label {
                text: qsTr("Select text file:")
            }
            TextField {
                id: fileEdit
                Layout.fillWidth: true
                text: ""
                placeholderText: qsTr("Select text file by browse button")
                readOnly: true
            }
            RoundButton {
                enabled: !processing
                radius: 4
                text: qsTr("Browse...")
                onClicked: fileDialog.open()
            }
        }
        RowLayout {
            Layout.fillWidth: true

            Label {
                text: qsTr("Number of words in histogram:")
            }
            SpinBox {
                id: wordsNumberSpinbox
                enabled: !processing
                editable: true
                from: 1
                to: 30
                value: 15
            }

            Label {
                text: qsTr("Delay of reading file(ms):")
            }
            SpinBox {
                id: delaySpinbox
                enabled: !processing
                editable: true
                from: 0
                to: 1000
                value: 100
            }
            RoundButton {
                Layout.preferredWidth: 100
                radius: 4
                font.bold: true
                enabled: processing || fileEdit.text.length > 0
                text: processing ? qsTr("Stop") : qsTr("Start")
                onClicked: processing ? stop() : start(fileEdit.text, wordsNumberSpinbox.value, delaySpinbox.value)
            }
        }

        Pane {
            Layout.fillWidth: true
            Layout.fillHeight: true
            padding: 0

            Label {
                anchors.centerIn: parent
                visible: !arrayModel.hasData
                text: qsTr("No data")
            }

            RowLayout {
                anchors.fill: parent
                visible: arrayModel.hasData

                Repeater {
                    model: arrayModel

                    Item {
                        Layout.fillWidth: true
                        Layout.fillHeight: true
                        Layout.alignment: Qt.AlignBottom
                        implicitHeight: wordLabel.implicitHeight + countLabel.implicitHeight

                        Label {
                            id: wordLabel
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.right: parent.right
                            horizontalAlignment: Text.AlignHCenter
                            elide: "ElideRight"
                            text: model.text
                        }

                        Rectangle {
                            id: histo
                            anchors.bottom: wordLabel.top
                            anchors.left: parent.left
                            anchors.right: parent.right
                            implicitHeight: (parent.height - wordLabel.height - countLabel.height) * (model.count / arrayModel.maxCount)
                            color: hashStringToColor(model.text)

                            Behavior on implicitHeight {
                                NumberAnimation { duration: 100 }
                            }
                        }
                        Label {
                            id: countLabel
                            anchors.bottom: histo.top
                            anchors.left: parent.left
                            anchors.right: parent.right
                            horizontalAlignment: Text.AlignHCenter
                            elide: "ElideRight"
                            text: model.count
                        }
                    }
                }
            }
        }

        ProgressBar {
            Layout.fillWidth: true
            from: 0
            to: 1
            value: progress

            Behavior on value {
                NumberAnimation { duration: 100 }
            }
        }
    }

    FileDialog {
        id: fileDialog
        nameFilters: ["All files (*.*)"]
        title: "Please choose a text file"
        folder: shortcuts.home
        modality: Qt.WindowModal
        selectExisting: true
        selectFolder: false
        selectMultiple: false
        onAccepted: {
            fileEdit.text = fileUrl
        }
    }

    function djb2(str){
      var hash = 5381;
      for (var i = 0; i < str.length; i++) {
        hash = ((hash << 5) + hash) + str.charCodeAt(i); /* hash * 33 + c */
      }
      return hash;
    }

    function hashStringToColor(str) {
      var hash = djb2(str);
      var r = (hash & 0xFF0000) >> 16;
      var g = (hash & 0x00FF00) >> 8;
      var b = hash & 0x0000FF;
      return "#" + ("0" + r.toString(16)).substr(-2) + ("0" + g.toString(16)).substr(-2) + ("0" + b.toString(16)).substr(-2);
    }
}
